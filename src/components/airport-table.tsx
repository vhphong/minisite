
export default function AirportTable(props: {
    iataCode: string, icaoCode: string, name: string,
    alpha2countryCode: string, latitude: number, longitude: number
}) {

    return (
        <div>
            {/* <table>
                <thead>
                    <th>IATA Code</th>
                    <th>ICAO Code</th>
                    <th>Name</th>
                    <th>Coutry Code</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                </thead>
                <tbody>
                    <tr>
                        <td>{props.iataCode}</td>
                        <td>{props.icaoCode}</td>
                        <td>{props.name}</td>
                        <td>{props.alpha2countryCode}</td>
                        <td>{props.latitude}</td>
                        <td>{props.longitude}</td>
                    </tr>
                </tbody>
            </table> */}
            <table>
                <tr>
                    <th>IATA Code</th>
                    <td>{props.iataCode}</td>
                </tr>
                <tr>
                    <th>ICAO Code</th>
                    <td>{props.icaoCode}</td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td>{props.name}</td>
                </tr>
                <tr>
                    <th>Coutry Code</th>
                    <td>{props.alpha2countryCode}</td>
                </tr>
                <tr>
                    <th>Latitude</th>
                    <td>{props.latitude}</td>
                </tr>
                <tr>
                    <th>Longitude</th>
                    <td>{props.longitude}</td>
                </tr>
            </table>
        </div>
    )

}