

export default function HowToIntro() {




    return (
        <div>
            <h2 style={{ "fontStyle": "italic" }}>Instructions:</h2>
            <ol>
                <li style={{ "fontStyle": "italic" }}>Click the button below to start</li>
                <li style={{ "fontStyle": "italic" }}>A new page will open to ask for an airport ICAO/IATA Code</li>
                <li style={{ "fontStyle": "italic" }}>Follow further instructions thereafter</li>
            </ol>
        </div>
    )

}