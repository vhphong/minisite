import axios from 'axios';
import { useState } from 'react';
import { useRef } from "react";
import { useDispatch } from "react-redux";
import { SyntheticEvent } from "react";
import { Airport } from '../dtos/airport';
import AirportTable from './airport-table';


export default function ICAOLookup() {

    const aviationInput = useRef<any>();

    const [airport, setAirport] = useState<Airport>();

    async function getAviationInfo(event: SyntheticEvent) {
        // read in value from input
        const inputValue: string = aviationInput.current.value;

        if (inputValue == "") {
            alert("Enter a valid airport ICAO or IATA code.");
        }


        // const response = await axios.get(``)

        var axios = require("axios").default;

        // specs of the api url, permission is needed when access
        var options = {
            method: 'GET',
            url: `https://aviation-reference-data.p.rapidapi.com/airports/${inputValue}`,
            headers: {
                'x-rapidapi-host': 'aviation-reference-data.p.rapidapi.com',
                'x-rapidapi-key': '68bf23e2a9msh9832dc09dead626p126d72jsndab89bb22cf8'
            }
        };

        const response = await axios.request(options);
        const airportData = await response.data;

        

        setAirport(airportData);
    }

    function Clear(event: SyntheticEvent) {
        // clear the input once the Clear button is pressed
        aviationInput.current.value = '';
        // window.location.reload();   // to reload the current page
        window.open("/view", "_self");
    }

    function openHomePage() {
        // window.open("http://localhost:3000", "_self");   // absolute path
        window.open("../", "_self");    // relative path, one level up
    }

    return (
        <div>
            <h4>Flights and Airports Look Up</h4>
            <input placeholder="Airport ICAO code" ref={aviationInput}></input>

            <button onClick={getAviationInfo}>Get Info</button>

            {airport === undefined ? <h1>No airport data found!</h1> : <AirportTable iataCode={airport.iataCode} icaoCode={airport.icaoCode} name={airport.name} alpha2countryCode={airport.alpha2countryCode} latitude={airport.latitude} longitude={airport.longitude}></AirportTable>}

            <button onClick={Clear}>Clear</button>

            {/* <button onClick={() => window.open("http://localhost:3000", "_self")}>Home</button> */}

            <button onClick={openHomePage}>Home</button>
        </div>
    )
}
