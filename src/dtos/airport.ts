

export interface Airport{
    iataCode: string;
    icaoCode:string;
    name: string;
    alpha2countryCode:string;
    latitude: number;
    longitude: number;
}